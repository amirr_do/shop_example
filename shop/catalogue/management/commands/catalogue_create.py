from __future__ import unicode_literals
# -*- coding: utf-8 -*-
import time
import json
import requests
import re
try:
    from urllib.request import urlretrieve
except ImportError:
    from urllib import urlretrieve
from io import BytesIO
from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from oscar.core.loading import get_class, get_model, get_classes

AttributeOption = get_model('catalogue', 'AttributeOption')
ProductAttributeValue = get_model('catalogue', 'ProductAttributeValue')
ProductAttribute = get_model('catalogue', 'ProductAttribute')
ProductClass = get_model('catalogue', 'ProductClass')
ProductCategory = get_model('catalogue', 'ProductCategory')
Category = get_model('catalogue', 'Category')
Product = get_model('catalogue', 'Product')
ProductImage = get_model('catalogue', 'ProductImage')
StockRecord = get_model('partner', 'StockRecord')
Partner = get_model('partner', 'Partner')


def func_time(func):
    def wrap(*args, **kwargs):
        t = time.time()
        result = func(*args, **kwargs)
        print("{}: {}".format(func.__name__, round(time.time()-t, 4)))
        return result

    return wrap


def adapt_category(fn):
    def wrapper():
        data = fn()
        return normalize_code(data)
    return wrapper


def normalize_code(data):
    for product in data:
        if str(product['category']) == 'CPU':
            product['category'] = 1
        elif str(product['category']) == 'Cooler':
            product['category'] = 2
        elif str(product['category']) == 'Motherboard':
            product['category'] = 3
        elif str(product['category']) == 'RAM':
            product['category'] = 4
        elif str(product['category']) == 'Videocard':
            product['category'] = 5
        elif str(product['category']) == 'HDD':
            product['category'] = 6
        elif str(product['category']) == 'SSD':
            product['category'] = 7
        elif str(product['category']) == 'ODD':
            product['category'] = 8
        elif str(product['category']) == 'Case':
            product['category'] = 9
        elif str(product['category']) == 'PSU':
            product['category'] = 10
    return data


def ld(p, encoding="utf8"):
    """загрузка объекта"""
    with open(p, "rt", encoding=encoding) as f:
        return json.load(f)


@adapt_category
def load_new_price():
    new_list = list()
    price = ld('test_price.json')
    for category_id in price.keys():
        params = price[category_id]
        for param in params:
            product_item = {
                'warranty': param["Warranty"],
                'code': param["Code"],
                'name': param["Name"],
                'category_name': param["CategoryName"],
                'category': category_id,
                'price_usd': param["PriceUSD"],
                'vendor': param["Vendor"],
                'article': param["Article"],
                'model': param["Model"],
                'stock': param["Stock"],
                'description': param["FullDescription"],
                'options': param["Options"],
                'info': param["Description"]
                }
            new_list.append(product_item.copy())
    return new_list


def get_price(price, attr):
    if attr['category'] == 1:
        return price*1.11
    elif attr['category'] == 2:
        return price*1.12
    elif attr['category'] == 3:
        return price*1.13
    elif attr['category'] == 4:
        return price*1.14
    elif attr['category'] == 5:
        return price*1.15
    elif attr['category'] == 6:
        return price*1.16
    elif attr['category'] == 7:
        return price*1.17
    elif attr['category'] == 8:
        return price*1.18
    elif attr['category'] == 9:
        return price*1.19
    elif attr['category'] == 10:
        return price*1.2


def get_cpu_core(c, options=None):
    pattern = '\d+(?=\s*яде?р)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        try:
            return int(found[0])
        except TypeError as err:
            print(err)
            return None
    else:
        print('Error: More or less values')
        return None


def get_cpu_socket(c, options=None):
    pattern = 's(1150|1155|2011);'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        return 'LGA{}'.format(found[0])

    pattern = 'AM3\+?|FM2\+?|[A|F]M1'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return found[0]

    return None


def get_cpu_box(c, options=None):
    pattern = '[Tt][Rr][Aa][Yy]'
    if not options:
        options = c.attr.options
    found = re.search(pattern, options)
    if found:
        return False

    pattern = 'BOX'
    found = re.search(pattern, options)
    if found:
        return True

    return None


def get_cpu_nm(c, options=None):
    pattern = '\d+nm(?=;)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        return found[0]
    else:
        return None


def get_cpu_power(c, options=None):
    pattern = '\d+(?=W;)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        return found[0]
    else:
        return None


def get_cpu_freq(c, options=None):
    pattern = '\d+[\.,]\d+\s*(?=GHz;)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        freq = float(found[0].replace(' ', '').replace(',', '.'))
        return freq
    return None


def get_cooler_socket(c, options=None):
    pattern = '(?<=;)\s*s?(1155|1150|2011|AM3\+?|[FA]M1|FM2\+?)\s{0,3}(?=;)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found):
        return found
    return None


def get_cooler_circulation(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s{0,3}(\d{1,5})\s{0,3}\-?\s{0,3}(\d{0,5})\s{0,3}(?=об[/минхв± 10%]*;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        circulation = found[0]
        if circulation[0] and circulation[1]:
            return '{}'.format('-'.join(circulation))
        else:
            return '{}'.format(circulation[0])
    return None


def get_cooler_dB(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s{0,3}[<>]{0,3}\s{0,3}(\d{1,3}[.]?\d{0,3})\s{0,3}[-]?\s{0,3}(\d{1,3}[.]?\d{0,3})?\s{0,3}(?=[dд][BBбБ];)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        dB = found[0]
        if dB[0] and dB[1]:
            return '{}'.format('-'.join(dB))
        else:
            return '{}'.format(dB[0])
    return None


def get_cooler_material(c, options=None):
    if not options:
        options = c.attr.options
    pattern = 'никелированная медь|медь|алюминий'
    found = re.findall(pattern, options)
    if found:
        return ', '.join(sorted(found))
    return None


def get_cooler_connect(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s*(\d)\-pin[^;]*(?=;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        circulation = found[0].replace(' ', '')
        return '{}'.format(circulation)
    return None


def get_cooler_type(c, options=None):
    if not options:
        options = c.attr.options
    pattern = 'жидкост'
    found = re.findall(pattern, options)
    if found:
        return 'жидкостное охлаждение'
    pattern = 'активная \(кулер\)'
    found = re.findall(pattern, options)
    if found:
        return 'активное охлаждение (кулер)'
    return 'пасивное охлаждение (радиатор)'


def get_motherboard_socket(c, options=None):
    pattern = '(?<=;\sSocket\s)(1155|1150|2011|AM3\+?|FM2\+?|[AF]M1)(?=;)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        socket = found[0].replace(' ', '')
        return '{}'.format(socket)
    pattern = 'С встроенным процессором'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return 'встроенный'
    return None


def get_motherboard_chipset(c, options=None):
    pattern = '(?<=;\sSocket\s)(1155|1150|2011|AM3\+?|FM2\+?|[AF]M1)(?=;);\s*([\w\d\s]+)(;.+)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        chipset = found[0][1]
        return '{}'.format(chipset)
    pattern = 'С встроенным процессором'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return None
    return None


def get_motherboard_size(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)([\w\d\s-]*[AI]TX[\w\d\s]*)(?=;)'
    found = re.findall(pattern, options)
    atx, mm = 'unknown', None
    if found:
        size = found[-1].strip()
        atx = '{}'.format(size)
        pattern = 'm[.]?ATX|E\s{0,3}ATX|Micro ATX'
        found = re.findall(pattern, atx, re.IGNORECASE)
        if found:
            atx = 'Micro-ATX'
        else:
            atx = 'Mini-ATX' if atx == 'Mini ATX' else atx
            atx = 'Mini-ITX' if atx == 'Mini ITX' else atx

    pattern = '(?<=;)([^;]*?[сc][мm])\s{0,3}(?=;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        pattern = '\d{1,3}[.]?\d{0,3}'
        f = re.findall(pattern, found[0])
        if len(f) == 2:
            mn = (int(float(f[0])*10), int(float(f[1])*10))
            mm = '{} x {}'.format(*mn)
            atx = get_correct_mb_atx(*mn)
    else:
        pattern = '(?<=;)([^;]*?[mм][мm])\s{0,3}(?=;)'
        found = re.findall(pattern, options)
        if len(found) == 1:
            pattern = '\d{1,4}'
            f = re.findall(pattern, found[0])
            if len(f) == 2:
                mn = (int(f[0]), int(f[1]))
                mm = '{} x {}'.format(*mn)
                atx = get_correct_mb_atx(*mn)
    if atx in ('Micro-ATX', 'Mini-ATX', 'ATX', 'Mini-ITX', 'Flex-ATX', 'Ultra-ATX', 'unknown'):
        return (atx, mm), True
    else:
        return (atx, mm), False


def get_ram_type(c, options=None):
    pattern = '(?<=; )(DDR\s*\d?\s*)(?=;)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        size = found[0].replace(' ', '')
        return '{}'.format(size)
    return None


def get_ram_qty_and_size(c, options=None):
    pattern = '(?<=; \(Kit: )(\d+)x(\d+)GB(?=\);)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        size = (int(found[0][0]), int(found[0][0])*int(found[0][1]))
        return size
    return None, None


def get_ram_frequency(c, options=None):
    pattern = '(?<=; )(\d+)\s*MHz(?=;)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        frequency = found[0]
        return int(frequency)

    pattern = '(\d+)\s*MHz(?=;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        frequency = found[0]
        return int(frequency)
    return None


def get_ram_latency(c, options=None):
    pattern = '(?<=; )(CL\d+)(\s*\([\d-]+\))?(?=;)'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        frequency = found[0][0]
        return '{}'.format(frequency)
    return None


def get_ram_radiator(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '\+\sрадиатор'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return 'радиатор'
    return 'нет'


def get_video_size_type_bit(c, options=None):
    pattern = '(?<=; )(\d+)\s*Mb;?.*;\s*(G?DDR[\sI\d]*);?.*;\s*(\d+)\s*Bit;'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        size, type_ram, bit = map(lambda x: x.replace(' ', ''), found[0])
        type_ram = 'GDDR3' if 'III' in type_ram else type_ram
        type_ram = 'G' + type_ram if type_ram[0] == 'D' else type_ram
        return int(size), type_ram, int(bit)
    return None, None, None


def get_video_cooling(c, options=None):
    pattern = 'С активным охлаждением'
    if not options:
        options = c.attr.options
    found = re.findall(pattern, options)
    if len(found) == 1:
        return '{}'.format(found[0])
    pattern = 'С пассивным охлаждением'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return '{}'.format(found[0])
    return None


def get_video_gpu(c, options=None):
    if not options:
        options = c.attr.options

    pattern = '(?<=;\s)\s*GeForce\s*(G?T?X?)\s*(\d)\d+\s?[\s\w\d]*;'
    found = re.findall(pattern, options)
    if len(found):
        return 'GeForce {}{}00 series'.format(found[-1][0], found[-1][1])

    pattern = '(?<=;\s)\s*Radeon\s*(R\d)[\d\w\s]+;'
    found = re.findall(pattern, options)
    if len(found):
        return 'Radeon {} series'.format(found[-1])
    pattern = '(?<=;\s)\s*Radeon\s*(HD\s*\d)[\d\w\s]*;'
    found = re.findall(pattern, options)
    if len(found):
        return 'Radeon {}000 series'.format(found[-1])

    pattern = 'Quadro|QUADRO'
    found = re.search(pattern, options)
    if found:
        return 'NVIDIA QUADRO'
    return None


def get_hdd_size(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s*(\d+)\s*[GГ][BВБ](?=;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        size = int(found[0])
        return size
    pattern = '(?<=;)\s*(\d+)\s*[TТ][BВБ](?=;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        size = int(float(found[0]) * 1000)
        return size
    return None


def get_hdd_form_factor(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s*(\d\.\d)\s*(?=";)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return '{}'.format(found[0])
    return None


def get_hdd_speed(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s*([\d-]+)\s*(?=об/мин;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return '{}'.format(found[0])
    pattern = '(?<=;)\s*(IntelliPower)\s*(?=;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return '{}'.format(found[0])
    return None


def get_hdd_buffer_size(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s*(\d+)\s*(?=MB;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return int(found[0])
    return None


def get_ssd_size(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s*(\d+)\s*GB(?=;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        size = int(found[0])
        return size
    pattern = '(?<=;)\s*(\d+)\s*TB(?=;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        size = int(float(found[0])*1000)
        return size
    return None


def get_ssd_interface(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s*(mSATA|SAS)\s*(?=;)'
    msata = re.findall(pattern, options)
    if len(msata):
        return '{}'.format(msata[-1])
    pattern = '(?<=;)\s*(SATA)\s*(.*?)(?=;)'
    sata = re.findall(pattern, options)
    if len(sata) == 1:
        t = 'III' if sata[0][1] == '3' or '6Gb/s' else sata[0][1]
        return '{} {}'.format(sata[0][0], t)
    pattern = '(?<=;)\s*(PCI-Express)\s*(?=;)'
    pci = re.findall(pattern, options)
    if len(pci) == 1:
        return '{}'.format(pci[0])
    return None


def get_ssd_io(c, options=None):
    if not options:
        options = c.attr.options
    io = [None, None]
    pattern = '(?<=;)(.*?)(\d+)\s{0,3}([MМ][bб]\s*/\s*[scс])(.*?)(?=;)'
    found = re.findall(pattern, options, re.IGNORECASE)
    if len(found) == 2:
        for i in range(len(found)):
            io[i] = int(found[i][1])
    elif len(found) == 1:
        io[0] = int(found[0][1])
    return io


def get_case_class(c, options=None):
    if not options:
        options = c.attr.options
    pattern = 'Классические|Мультимедийные|Компактные|Геймерские|Для неттопов|Серверные'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return found[0]
    return None


def get_case_type(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s*([^;]*?)([Tt]ower)\s*(?=;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        if len(found[0]) == 2:
            return '{}{}'.format(found[0][0], found[0][1])
    pattern = '(?<=;)\s*(Slim|HTPC)\s*(?=;)'
    found = re.findall(pattern, options, re.IGNORECASE)
    if len(found) == 1:
        return '{}'.format(found[0])
    pattern = '(?<=;)\s*(ATX)\s*(?=;)'
    found = re.findall(pattern, options)
    if len(found):
        return 'Miditower'
    return None


def get_case_form_factor(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;|,)\s*([Minicroe.XL\-]*[IA]TX)(?=;|,)'
    found = re.findall(pattern, options, re.IGNORECASE)
    if found:
        return sorted(list(set(found)))
    return None


def get_case_power(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s*(\d+)\s*(?=Вт;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return int(found[0])
    if 'Без блока питания' in options:
        return 0
    return None


def get_psu_power(c, options=None):
    if not options:
        options = c.attr.options
    pattern = '(?<=;)\s*(\d+)\s*(?=Вт;)'
    found = re.findall(pattern, options)
    if len(found) == 1:
        return int(found[0])
    return None
'''
=== Main block ===
'''


def update_or_create_product(self, attributes, code):
    name = attributes['name'].replace('"', "'")
    description = attributes['description'].replace('"', "'")
    product_class = ProductClass.objects.get(pk=int(attributes['category']))

    product, created = Product.objects.get_or_create(upc=code, defaults={
        'structure': 'standalone',
        'title': name,
        'slug': code,
        'description': description,
        'is_discountable': True,
        'product_class': product_class
    })

    if created:
        create_product(self, product, attributes)
    else:
        product.description = description
        product.save()

    return product, created


# @func_time
def create_product(self, product, attributes):

    # TODO
    # set_images(product, more_info)

    set_attributes(product, attributes)


# TODO
# def set_images(product, info):
#     images = get_images(info['large_image'])
#     if images:
#         create_product_image(product, images)
#
#
# def get_images(image):
#     images = list()
#     image_item = image
#     i = 2
#     while str(requests.get(image_item).status_code) == '200':
#         images.append(image_item)
#         image_item = image.replace('big', '{}big'.format(i))
#         i += 1
#     return images
#
#
# def create_product_image(product, images):
#     for i, link in enumerate(images):
#         image = requests.get(link)
#         img = BytesIO(image.content)
#
#         image_name = link.split('/')[-1]
#         image_file = File(img)
#
#         product_image = ProductImage.objects.create(product=product, display_order=i)
#         product_image.original.save(image_name, image_file)


def get_correct_mb_atx(height, width):
    if 307 > height > 285:
        if width < 247:
            return 'ATX'
        else:
            return 'Ultra-ATX'
    if height > 245:
        if width < 208:
            return 'Mini-ATX'
        elif width < 246:
            return 'ATX'
    if height > 170:
        if width > 191:
            return 'Micro-ATX'
        else:
            return 'Flex-ATX'
    if height < 171:
        return 'Mini-ITX'
    return 'Ultra-ATX'


def set_attributes(product, attributes):
    main_attributes = {
        'warranty':  attributes['warranty'],
        'vendor': attributes['vendor'],
        'article': attributes['article'],
        'model': attributes['model'],
        'options': attributes['options']
    }
    for key in main_attributes.keys():
        ProductAttributeValue.objects.get_or_create(product=product,
                                                    attribute=ProductAttribute.objects.get(code=key), defaults={
                                                        'value_text': main_attributes[key]})
    product_attributes = dict()
    if attributes['category'] == 1:
        product_attributes = {
            'core': get_cpu_core(product, main_attributes['options']),
            'socket': get_cpu_socket(product, main_attributes['options']),
            'tray': get_cpu_box(product, main_attributes['options']),
            'nm': get_cpu_nm(product, main_attributes['options']),
            'power': get_cpu_power(product, main_attributes['options']),
            'freq': get_cpu_freq(product, main_attributes['options'])
            }
    elif attributes['category'] == 2:
        product_attributes = {
            'circulation': get_cooler_circulation(product, main_attributes['options']),
            'dB': get_cooler_dB(product, main_attributes['options']),
            'material': get_cooler_material(product, main_attributes['options']),
            'connect': get_cooler_connect(product, main_attributes['options']),
            'type_cooler': get_cooler_type(product, main_attributes['options'])
        }
    elif attributes['category'] == 3:
        size, _ = get_motherboard_size(product, main_attributes['options'])
        product_attributes= {
            'socket': get_motherboard_socket(product, main_attributes['options']),
            'chipset': get_motherboard_chipset(product, main_attributes['options']),
            'form_factor': size[0],
            'size': size[1],
        }
    elif attributes['category'] == 4:
        qty, volume = get_ram_qty_and_size(product, main_attributes['options'])
        product_attributes = {
            'type_ram': get_ram_type(product, main_attributes['options']),
            'frequency': get_ram_frequency(product, main_attributes['options']),
            'latency': get_ram_latency(product, main_attributes['options']),
            'radiator': get_ram_radiator(product, main_attributes['options']),
            'qty': qty,
            'volume': volume,
        }
    elif attributes['category'] == 5:
        volume_memory, video_type, bit = get_video_size_type_bit(product, main_attributes['options'])
        product_attributes = {
            'cooling': get_video_cooling(product, main_attributes['options']),
            'gpu': get_video_gpu(product, main_attributes['options']),
            'volume_memory': volume_memory,
            'video_type': video_type,
            'bit': bit,
        }
    elif attributes['category'] == 6:
        product_attributes = {
            'volume': get_hdd_size(product, main_attributes['options']),
            'form_factor': get_hdd_form_factor(product, main_attributes['options']),
            'speed': get_hdd_speed(product, main_attributes['options']),
            'buffer_size': get_hdd_buffer_size(product, main_attributes['options']),
        }
    elif attributes['category'] == 7:
        read, write = get_ssd_io(product, main_attributes['options'])
        product_attributes = {
            'volume': get_ssd_size(product, main_attributes['options']),
            'interface': get_ssd_interface(product, main_attributes['options']),
            'read_speed': read,
            'write_speed': write,
        }
    elif attributes['category'] == 9:
        product_attributes = {
            'class_case': get_case_class(product, main_attributes['options']),
            'type_case': get_case_type(product, main_attributes['options']),
            'power': get_case_power(product, main_attributes['options']),
        }
    elif attributes['category'] == 10:
        product_attributes = {
            'power': get_psu_power(product, main_attributes['options'])
        }
    add_attr(product, product_attributes)
    print_none_attr(product, product_attributes, main_attributes['options'])


def add_attr(product, attr):
    for key in attr.keys():
        if attr[key] is not None:
            setattr(product.attr, key, attr[key])
    product.attr.save()


def print_none_attr(product, attr, options=None):
    if not options:
        options = product.attr.options
    attr_none = []
    for k in attr.keys():
        if attr[k] is None:
            attr_none.append(k)

    if attr_none:
        print()
        print([k for k in attr_none])

        print(product.pk, product.upc, options)


def product_stock_record(product, attr):
    first_partner = Partner.objects.get(code='first_partner')
    second_partner = Partner.objects.get(code='second_partner')
    code = attr['code']
    stock = attr["stock"]
    price_first = float(attr['price_usd'])
    price_second = get_price(price_first, attr)

    partner_stockrecord, _ = StockRecord.objects.update_or_create(
        product=product, partner=first_partner, defaults={
            'partner_sku': code,
            'price_excl_tax': price_first,
            'num_in_stock': stock*20,
            'price_currency': 'UAH'
        })

    partner_stockrecord, _ = StockRecord.objects.update_or_create(
        product=product, partner=second_partner, defaults={
            'partner_sku': code,
            'price_excl_tax': price_second,
            'num_in_stock': stock*20,
            'price_currency': 'UAH'
        })


class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        if 'db' in args:
            for product_attr in load_new_price():

                # Product Info
                product, created = update_or_create_product(self, product_attr, product_attr['code'])

                # Stock Record
                product_stock_record(product, product_attr)

                # Product Category
                if created:
                    category = Category.objects.get(pk=int(product_attr['category']) + 1)
                    ProductCategory.objects.get_or_create(category=category, product=product)
